﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Gluid
{
    /// <summary>
    /// A special representation of a Guid that can contain (or internalise) one or two 4-byte integers or a single long in a reversible way against a namespace
    /// The Guid uses a reserved version number (0xf) to ensure it doesn't clash with most system generated Guids.
    /// Beyond that it's up to you to ensure that your integers and namespaces are unique.
    /// </summary>
    public class Gluid
    {
        private Guid _subjectGuid;

        private int _version;
        private byte[] _guidBytes;

        /// <summary>
        /// Createa a new Gluid using the standard Guid.NewGuid method. This Gluid will not be linked to any other Id or Namespace
        /// </summary>
        /// <returns>A Gluid contains a standard Guid</returns>
        public static Gluid NewGluid()
            => new Gluid(Guid.NewGuid());

        /// <summary>
        /// Create a Guid from a Namespace and existing Id value
        /// </summary>
        /// <param name="namespace">A string to uniquely seperate regular Id values</param>
        /// <param name="value">The original Id value</param>
        /// <returns>A system Guid</returns>
        public static Guid NewGuid(string @namespace, int value)
            => new Gluid(@namespace, value).ToGuid();

        /// <summary>
        /// Create a Guid from a Namespace and two existing Id values
        /// </summary>
        /// <param name="namespace">A string to uniquely seperate regular Id values</param>
        /// <param name="value1">One of the original Id values</param>
        /// <param name="value2">One of the original Id values</param>
        /// <returns>A system Guid</returns>
        public static Guid NewGuid(string @namespace, int value1, int value2)
            => new Gluid(@namespace, value1, value2).ToGuid();

        /// <summary>
        /// Create a Guid from a Namespace and existing Id value
        /// </summary>
        /// <param name="namespace">A string to uniquely seperate regular Id values</param>
        /// <param name="value">The original Id value</param>
        /// <returns>A system guid</returns>
        public static Guid NewGuid(string @namespace, long value)
            => new Gluid(@namespace, value).ToGuid();

        /// <summary>
        /// Construct a new Gluid based on an existing Guid.
        /// </summary>
        /// <param name="guid">Guid to base the Gluid on</param>
        public Gluid(Guid guid)
        {
            _subjectGuid = guid;

            _guidBytes = _subjectGuid.ToByteArray();
            _version = (_guidBytes[7] & 0xf0) >> 4;
        }

        /// <summary>
        /// Construct a Gluid from a Namespace and existing Id value
        /// </summary>
        /// <param name="namespace">A string to uniquely seperate regular Id values</param>
        /// <param name="value">The original Id value</param>
        public Gluid(string @namespace, int value)
            : this(@namespace, value, 0) { }
        
        /// <summary>
        /// Construct a Gluid from a Namespace and two existing Id values
        /// </summary>
        /// <param name="namespace">A string to uniquely seperate regular Id values</param>
        /// <param name="value1">One of the original Id values</param>
        /// <param name="value2">One of the original Id values</param>
        public Gluid(string @namespace, int value1, int value2)
        {
            _version = 15;
            var namespaceHash = GetHash(@namespace);

            var intBytes1 = BitConverter.GetBytes(value1);
            var intBytes2 = BitConverter.GetBytes(value2);

            var guidBytes = new byte[16];

            PutLowerBytes(intBytes1, guidBytes);
            PutUpperBytes(intBytes2, guidBytes, 15);

            Buffer.BlockCopy(namespaceHash, 0, guidBytes, 9, 7);

            _subjectGuid = new Guid(guidBytes);
            _guidBytes = _subjectGuid.ToByteArray();
        }

        /// <summary>
        /// Construct a Gluid from a Namespace and existing Id value
        /// </summary>
        /// <param name="namespace">A string to uniquely seperate regular Id values</param>
        /// <param name="value">The original Id value</param>
        public Gluid(string @namespace, long value)
        {
            _version = 15;
            var namespaceHash = GetHash(@namespace);

            var longBytes = BitConverter.GetBytes(value);
            var upperBytes = new byte[4];

            Buffer.BlockCopy(longBytes, 4, upperBytes, 0, 4);

            var guidBytes = new byte[16];

            PutLowerBytes(longBytes, guidBytes);
            PutUpperBytes(upperBytes, guidBytes, 15);

            Buffer.BlockCopy(namespaceHash, 0, guidBytes, 9, 7);

            _subjectGuid = new Guid(guidBytes);
            _guidBytes = _subjectGuid.ToByteArray();
        }

        /// <summary>
        /// Indicates if this Gluid is linked to a regular Id value
        /// </summary>
        /// <returns>A bool indicating if the Gluid has a regular Id value</returns>
        public bool IsLinked() => _version == 15;

        /// <summary>
        /// Indicates if this Gluid is linked to a regular Id value on the specified namespace
        /// </summary>
        /// <param name="namespace">The namespace to check</param>
        /// <returns>A bool indicating if the Gluid has a regular Id value in the namespace</returns>
        public bool IsLinked(string @namespace)
        {
            if (!IsLinked())
            {
                return false;
            }

            var encHash = GetHash(@namespace);

            var i = 9;
            foreach (var hashByte in encHash)
            {
                if (_guidBytes[i] != hashByte)
                {
                    return false;
                }
                i++;
            }

            return true;
        }

        /// <summary>
        /// Get the associated original Id from the Gluid
        /// </summary>
        /// <returns>The original Int32 Id or null if this is just a regular Guid</returns>
        public int? ToInt32()
            => IsLinked() ? BitConverter.ToInt32(GetLowerBytes(_guidBytes), 0) : (int?)null;

        /// <summary>
        /// Get the associated original Id from the Gluid based on a provided namespace
        /// </summary>
        /// <param name="namespace">The namespace to filter the Id against</param>
        /// <returns>The original Int32 Id or null if this is a regular Guid or doesn't come from the supplied namespace</returns>
        public int? ToInt32(string @namespace)
            => IsLinked(@namespace) ? BitConverter.ToInt32(GetLowerBytes(_guidBytes), 0) : (int?)null;

        /// <summary>
        /// Get the associated original long Id from the Gluid
        /// </summary>
        /// <returns>The original Int64 Id or null if this is just a regular Guid</returns>
        public long? ToInt64()
            => IsLinked() ? GetInt64() : (long?)null;

        /// <summary>
        /// Get the associated original long Id from the Gluid based on a provided namespace
        /// </summary>
        /// <param name="namespace">The namespace to filter the Id against</param>
        /// <returns>The original Int64 Id or null if this is a regular Guid or doesn't come from the supplied namespace</returns>
        public long? ToInt64(string @namespace)
            => IsLinked(@namespace) ? GetInt64() : (long?)null;

        /// <summary>
        /// Get the associated original second int Id from the Gluid
        /// </summary>
        /// <returns>The original second int32 Id or null if this is just a regular Guid</returns>
        public int? GetSecondInt32()
            => IsLinked() ? BitConverter.ToInt32(GetUpperBytes(_guidBytes), 0) : (int?)null;

        /// <summary>
        /// Get the associated original second int Id from the Gluid
        /// </summary>
        /// <param name="namespace">The namespace to filter the Id against</param>
        /// <returns>The original second int32 Id or null if this is just a regular Guid</returns>
        public int? GetSecondInt32(string @namespace)
            => IsLinked(@namespace) ? BitConverter.ToInt32(GetUpperBytes(_guidBytes), 0) : (int?)null;

        /// <summary>
        /// Get the Gluid as a regular Guid type
        /// </summary>
        /// <returns>A Guid of the Gluid</returns>
        public Guid ToGuid() => _subjectGuid;

        /// <summary>
        /// Compare if the provided Gluid is equal to this one
        /// </summary>
        /// <param name="obj">Gluid to compare</param>
        /// <returns>true if equal</returns>
        public bool Equals(Gluid obj) => _subjectGuid == obj.ToGuid();

        /// <summary>
        /// Compare if the provided Guid is equal to this Gluid
        /// </summary>
        /// <param name="obj">Guid to compare</param>
        /// <returns>true if equal</returns>
        public bool Equals(Guid obj) => _subjectGuid == obj;

        /// <inheritdoc />
        public override string ToString() => _subjectGuid.ToString();
        
        /// <inheritdoc />
        public override int GetHashCode() => _subjectGuid.GetHashCode();

        private long GetInt64()
        {
            var longBytes = new byte[8];

            Buffer.BlockCopy(GetLowerBytes(_guidBytes), 0, longBytes, 0, 4);
            Buffer.BlockCopy(GetUpperBytes(_guidBytes), 0, longBytes, 4, 4);

            return BitConverter.ToInt64(longBytes, 0);
        }

        private byte[] GetLowerBytes(byte[] guidBytes)
        {
            var lowerBytes = new byte[4];

            lowerBytes[0] = guidBytes[0];
            lowerBytes[1] = guidBytes[1];
            lowerBytes[2] = guidBytes[2];
            lowerBytes[3] = guidBytes[3];

            return lowerBytes;
        }

        private byte[] GetUpperBytes(byte[] guidBytes)
        {
            var upperBytes = new byte[4];

            upperBytes[0] = guidBytes[4];
            upperBytes[1] = guidBytes[5];
            upperBytes[2] = (byte)(((guidBytes[7] & 0x0f) << 4) | (guidBytes[8] & 0x0f));
            upperBytes[3] = guidBytes[6];

            return upperBytes;
        }

        private void PutLowerBytes(byte[] lowerBytes, byte[] guidBytes)
        {
            guidBytes[0] = lowerBytes[0];
            guidBytes[1] = lowerBytes[1];
            guidBytes[2] = lowerBytes[2];
            guidBytes[3] = lowerBytes[3];
        }

        private void PutUpperBytes(byte[] upperBytes, byte[] guidBytes, int version)
        {
            var versionByte = (byte)((version & 0x0f) << 4);

            guidBytes[4] = upperBytes[0];
            guidBytes[5] = upperBytes[1];
            guidBytes[7] = (byte)(versionByte | ((upperBytes[2] & 0xf0) >> 4));
            guidBytes[6] = upperBytes[3];
            guidBytes[8] = (byte)(upperBytes[2] & 0x0f);
        }

        private byte[] GetHash(string @namespace)
        {
            var hashArray = new byte[7];
            var encArray = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(@namespace));

            Buffer.BlockCopy(encArray, 0, hashArray, 0, 7);
            
            return hashArray;
        }
    }

    /// <summary>
    /// Extension methods to the standard Guid to simplify Gluid interaction
    /// </summary>
    public static class GuidExtensions
    {
        /// <summary>
        /// Get the internal Int32 from the current Guid if it is a Gluid
        /// </summary>
        /// <param name="guid">The Guid to use</param>
        /// <returns>An Int32 from the Gluid or Null if this isn't a Gluid</returns>
        public static int? ToInt32(this Guid guid)
            => new Gluid(guid).ToInt32();

        /// <summary>
        /// Get the internal Int32 from the current Guid and namespace if it is a Gluid
        /// </summary>
        /// <param name="guid">The Guid to use</param>
        /// <param name="namespace">The namespace to get the Int32 from</param>
        /// <returns>An Int32 from tne Gluid or Null if the namespace doesn't match the Gluid or this isn't a Gluid</returns>
        public static int? ToInt32(this Guid guid, string @namespace)
            => new Gluid(guid).ToInt32(@namespace);

        /// <summary>
        /// Get the internal Int64 from the current Guid if it is a Gluid
        /// </summary>
        /// <param name="guid">The Guid to use</param>
        /// <returns>An Int64 from the Gluid or Null if this isn't a Gluid</returns>
        public static long? ToInt64(this Guid guid)
            => new Gluid(guid).ToInt64();

        /// <summary>
        /// Get the internal Int64 from the current Guid and namespace if it is a Gluid
        /// </summary>
        /// <param name="guid">The Guid to use</param>
        /// <param name="namespace">The namespace to get the Int64 from</param>
        /// <returns>An Int64 from tne Gluid or Null if the namespace doesn't match the Gluid or this isn't a Gluid</returns>
        public static long? ToInt64(this Guid guid, string @namespace)
            => new Gluid(guid).ToInt64(@namespace);

        /// <summary>
        /// Get the second internal Int32 from the current Guid if it is a Gluid and has two internal Int32 values
        /// Note: If the Gluid contains a long (Int64) value, this extension will return the upper 4 bytes of the long
        /// </summary>
        /// <param name="guid">The Guid to use</param>
        /// <returns>An Int32 from the Gluid or Null if this isn't a Gluid</returns>
        public static int? GetSecondInt32(this Guid guid)
            => new Gluid(guid).GetSecondInt32();

        /// <summary>
        /// Get the second internal Int32 from the current Guid and namespace if it is a Gluid and has two internal Int32 values
        /// Note: If the Gluid contains a long (Int64) value, this extension will return the upper 4 bytes of the long
        /// </summary>
        /// <param name="guid">The Guid to use</param>
        /// <param name="namespace">The namespace to get the Int32 from</param>
        /// <returns>An Int32 from tne Gluid or Null if the namespace doesn't match the Gluid or this isn't a Gluid</returns>
        public static int? GetSecondInt32(this Guid guid, string @namespace)
            => new Gluid(guid).GetSecondInt32(@namespace);

        /// <summary>
        /// Is this Guid a Gluid with an internal value
        /// </summary>
        /// <param name="guid">The Guid to use</param>
        /// <returns>Returns true if this is a Gluid with an internal value</returns>
        public static bool IsLinked(this Guid guid)
            => new Gluid(guid).IsLinked();

        /// <summary>
        /// Is this Guid a Gluid with an internal value within the specified namespace
        /// </summary>
        /// <param name="guid">The Guid to use</param>
        /// <param name="namespace">The namespace to check</param>
        /// <returns>Returns true if this is a Gluid with an internal value in the specified namespace</returns>
        public static bool IsLinked(this Guid guid, string @namespace)
            => new Gluid(guid).IsLinked(@namespace);
    }
}
