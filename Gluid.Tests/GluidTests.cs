using FluentAssertions;
using System;
using Xunit;
using FizzWare.NBuilder;
using System.Security.Cryptography;
using System.Text;

namespace Gluid.Tests
{
    public class GluidTests
    {
        public class TheNewGuidMethod
        {
            [Fact]
            public void ShouldCreateAValidGuid_WhenGivenANamespaceAndIntValue()
            {
                // Arrange
                var @namespace = "Test 1";
                var id = 1;
                var expected = new Gluid(@namespace, id).ToGuid();

                // Act
                var testGuid = Gluid.NewGuid(@namespace, id);
                
                testGuid.Should().Be(expected);
            }

            [Fact]
            public void ShouldCreateAValidGuid_WhenGivenANamespaceAndTwoIntValues()
            {
                // Arrange
                var @namespace = "Test 2";
                var id1 = 1;
                var id2 = 2;
                var expected = new Gluid(@namespace, id1, id2).ToGuid();

                // Act
                var testGuid = Gluid.NewGuid(@namespace, id1, id2);

                testGuid.Should().Be(expected);
            }

            [Fact]
            public void ShouldCreateAValidGuid_WhenGivenANamespaceAndALong()
            {
                // Arrange
                var @namespace = "Test 3";
                var id = long.MaxValue;
                var expected = new Gluid(@namespace, id).ToGuid();

                // Act
                var testGuid = Gluid.NewGuid(@namespace, id);

                testGuid.Should().Be(expected);
            }
        }

        public class TheNewGluidMethod
        {
            [Fact]
            public void ShouldCreateAGluidWithASystemGuid_WhenCalled()
            {
                // Act
                var testGluid = Gluid.NewGluid();

                testGluid.Should().BeOfType<Gluid>();
                testGluid.IsLinked().Should().Be(false);
            }
        }

        public class TheGluidConstructor
        {
            [Fact]
            public void ShouldCreateAGluidOfGUid_WhenCalledWithAGuid()
            {
                // Arrange
                var testGuid = Guid.NewGuid();

                // Act
                var testGluid = new Gluid(testGuid);

                // Assert
                testGluid.ToString().Should().Be(testGuid.ToString());
            }

            [Fact]
            public void ShouldCreateAGluidOfInt_WhenCalledWithANamespaceAndAnInt()
            {
                // Arrange
                var testInt = new Random().Next();
                var @namespace = "Test 4";

                var testIntBytes = BitConverter.GetBytes(testInt);

                // Act
                var testGluid = new Gluid(@namespace, testInt);

                // Assert
                var testGuid = testGluid.ToGuid();
                var guidInt = BitConverter.ToInt32(GetLowerBytes(testGuid));

                TestGluidNamespace(testGuid, @namespace).Should().BeTrue();
                guidInt.Should().Be(testInt);
            }

            [Fact]
            public void ShouldCreateAGluidOfTwoInts_WhenCalledWithANamespaceAndTwoInts()
            {
                // Arrange
                var rand = new Random();
                var testInt1 = rand.Next();
                var testInt2 = rand.Next();
                var @namespace = "Test 5";

                var testInt1Bytes = BitConverter.GetBytes(testInt1);
                var testInt2Bytes = BitConverter.GetBytes(testInt2);

                // Act
                var testGluid = new Gluid(@namespace, testInt1, testInt2);

                // Assert
                var testGuid = testGluid.ToGuid();
                var guidInt1 = BitConverter.ToInt32(GetLowerBytes(testGuid));
                var guidInt2 = BitConverter.ToInt32(GetUpperBytes(testGuid));

                TestGluidNamespace(testGuid, @namespace).Should().BeTrue();
                guidInt1.Should().Be(testInt1);
                guidInt2.Should().Be(testInt2);
            }

            [Fact]
            public void ShouldCreateAGluidOfLong_WhenCalledWithANamespaceAndALong()
            {
                // Arrange
                var testLong = (long)(new Random().Next() * 10000000000);
                var @namespace = "Test 4";

                var testLongBytes = BitConverter.GetBytes(testLong);

                // Act
                var testGluid = new Gluid(@namespace, testLong);

                // Assert
                var testGuid = testGluid.ToGuid();
                var guidLongBytes = new byte[7];
                Buffer.BlockCopy(GetLowerBytes(testGuid), 0, guidLongBytes, 0, 4);
                Buffer.BlockCopy(GetUpperBytes(testGuid), 0, guidLongBytes, 4, 4);
                var guidLong = BitConverter.ToInt64(guidLongBytes);

                TestGluidNamespace(testGuid, @namespace).Should().BeTrue();
                guidLong.Should().Be(testLong);
            }

            private byte[] GetLowerBytes(Guid subjectGuid)
            {
                var bytes = subjectGuid.ToByteArray();

                var lowerBytes = new byte[3];
                Buffer.BlockCopy(bytes, 0, lowerBytes, 0, 4);

                return lowerBytes;
            }

            private byte[] GetUpperBytes(Guid subjectGuid)
            {
                var guidBytes = subjectGuid.ToByteArray();

                var upperBytes = new byte[4];

                upperBytes[0] = guidBytes[4];
                upperBytes[1] = guidBytes[5];
                upperBytes[2] = (byte)(((guidBytes[7] & 0x0f) << 4) | (guidBytes[8] & 0x0f));
                upperBytes[3] = guidBytes[6];

                return upperBytes;
            }

            private bool TestGluidNamespace(Guid subjectGuid, string @namespace)
            {
                var guidBytes = subjectGuid.ToByteArray();

                var hashArray = new byte[7];
                var encArray = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(@namespace));

                Buffer.BlockCopy(encArray, 0, hashArray, 0, 7);

                var i = 9;
                foreach (var hashByte in hashArray)
                {
                    if (guidBytes[i] != hashByte)
                    {
                        return false;
                    }
                    i++;
                }

                return true;
            }
        }

        public class TheIsLinkedMethod
        {
            [Fact]
            public void ShouldReturnTrue_WhenTheGluidIsAGluid()
            {

            }

            [Fact]
            public void ShouldReturnFalse_WhenTheGluidIsARegularGuid()
            {

            }

            [Fact]
            public void ShouldReturnTrue_WhenTheGluidIsAGluidAndTheNamespaceMatches()
            {

            }

            [Fact]
            public void ShouldReturnFalse_WhenTheGluidIsAGluidAndTheNamespaceDoesNotMatch()
            {

            }
        }

        public class TheToInt32Method
        {

        }

        public class TheToInt64Method
        {

        }

        public class TheGetSecondInt32Method
        {

        }

        public class TheToGuidMethod
        {

        }

        public class TheToStringMethod
        {

        }

        public class TheEqualsMethod
        {

        }

        public class TheGetHashCodeMethod
        {

        }
    }
}
